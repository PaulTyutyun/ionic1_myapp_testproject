angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider



      .state('menu.home', {
    url: '/page1',
    views: {
      'side-menu21': {
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl'
      }
    }
  })
  .state('menu.lists', {
    url: '/page4',
    views: {
      'side-menu21': {
        templateUrl: 'templates/lists.html',
        controller: 'cartCtrl'
      }
    }
  })

    .state('menu.lists2', {
      url: "/page5",
      views: {
        'side-menu21': {
          templateUrl: 'templates/lists2.html',
          controller: 'cartCtrl'
        }
      }
    })

    .state('menu.cards', {
      url:"/page6",
      views: {
        'side-menu21': {
          templateUrl: 'templates/cards.html',
          controller: 'cartCtrl'
        }
      }
  })

    .state('menu.cart', {
    url: '/page2',
    views: {
      'side-menu21': {
        templateUrl: 'templates/cart.html',
        controller: 'cartCtrl'
      }
    }
  })


  .state('menu.cloud', {
    url: '/page3',
    views: {
      'side-menu21': {
        templateUrl: 'templates/cloud.html',
        controller: 'cloudCtrl'
      }
    }
  })

  .state('menu', {
    url: '/side-menu21',
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl'
  })

$urlRouterProvider.otherwise('/side-menu21/page1')



});
